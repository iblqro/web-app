/*
Author: Mazhar Iqbal
Module: Work Shift Panel      
*/

//Add Work Shift
import React, { useRef } from "react";
import { Modal } from "react-bootstrap";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
import { CreateCompanyWorkShift, GetAllWorkShifts, UpdateWorkShiftName } from "../../../../../reduxToolkit/CompanyWorkShift/CompanyWorkShiftApi";
import Cookies from "js-cookie";
import { useTranslation } from 'react-i18next'
import { useState } from "react";
import { useEffect } from "react";


const AddWorkShift = (props) => {
  console.log(props?.data)
  const { t } = useTranslation();
  const lCode = Cookies.get("i18next") || "en";
  const [workshiftName, setWorkShiftName] = useState("")

  // use hook importer
  const dispatch = useDispatch();

  // create work shift
  const handleSubmit = () => {
    if(props?.isUpdate){
      const payload = {
        id:props?.data?.id,
        name:workshiftName
      }
      dispatch(UpdateWorkShiftName(payload)).then(res =>{
        console.log(res)
        if(res.payload?.data?.code ===200){
          props.onHide()
          toast.success("Successfuly Updated")
        }
      })


    }else{
      if (workshiftName) {
        dispatch(CreateCompanyWorkShift(workshiftName))
          .then(() => {
            props.onHide()
          })
      } else {
        toast.info("Please Add Sift Name")
      }
    }
   
  }

  useEffect(() => {
    if (props?.isUpdate) {
      setWorkShiftName(props?.data?.name)
    }

  }, [props?.data?.name])
  return (
    <Modal
      {...props}
      size="md"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      style={{ background: "rgba(0,0,0,0.5)" }}
    >
      <button onClick={props.onHide} className="modal-close-btn">
        X
      </button>
      <span className="main-modal-heading">{props?.isUpdate ? t("change_name") : t('add_new_work_shift')}</span>
      <div className="unlink-modal-body">

        {
          props.isUpdate ?
            <span
              className="modal-desc-text"
              style={{ color: "#707070", fontSize: "12px", fontWeight: "normal", textAlign: "left" }}
            >
              Enter the new name for the work shift: <span style={{fontWeight:"bold"}}>{props?.data?.name}</span>

            </span> :

            <span
              className="modal-desc-text"
              style={{ color: "#707070", fontSize: "12px", fontWeight: 400, textAlign: "left" }}
            >
              Enter the name of work shift

            </span>
        }
        <div className="mt-3" style={{ position: "relative" }}>
          <label className="rejection-note-label">{t('work_shift_panel')}</label>
          <input
            style={{
              height: "45px",
              borderRadius: "12px", paddingLeft: "14px"
            }}
            value={workshiftName}
            onChange={(e) => setWorkShiftName(e.target.value)}
            type="tex"
            id="message"
            name="message"
            className="rejection-note-field w-100"
          />
        </div>
        <div className="btn-div">
          <button
            className="custom_btn_cancel_gray_hover"
            style={{ width: '100%' }}
            onClick={props.onHide}
          >
            {t('cancel')}
          </button>
          <button
            className="custom_primary_btn_dark"
            style={{ width: '100%' }}
            onClick={() => { handleSubmit() }}
          >
            {props?.isUpdate ? t("update").toUpperCase() : t('create')}
          </button>
        </div>
      </div>
    </Modal>
  );
};

export default AddWorkShift;

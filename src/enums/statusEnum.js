export const status = {
    1: "pre_registered",
    2: "to_change_password",
    3: "to_approve_documentS",
    4: "active",
    5: "on_vacations",
    6: "in_active",
    21: "contract_finished",
    22: "contract_active",
    23: "contract_cancel"
}
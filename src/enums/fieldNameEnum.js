import { deviceType } from "./deviceTypeEnum";
import { gender } from "./genderEnum";
import { status } from "./statusEnum";

export const fieldNameEnum = {
    deviceType,
    gender,
    status
}
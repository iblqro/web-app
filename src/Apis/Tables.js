export const TABLES = {
    DEVICES: 'device',
    EMPLOYEES: 'employee',
    CONTRACTORS: 'contractor',
    CONTRACTS: 'contract'

    // Add more tables here
  };